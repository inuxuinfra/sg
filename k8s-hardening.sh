#!/bin/bash

# Functions

put_title(){
        echo -e "\033[7m $* \033[0m"
}

put_msg(){
        echo -e "\e[0;33m $* \033[0m"
}

put_warn(){
        echo -e "\e[0;31m $* \033[0m"
}

put_inst(){
        echo -e "\e[0;35m $* \033[0m"
}



HOSTNAME="server-name"
#DOMAIN="domain"
PUB_IP=$(/sbin/ifconfig eth0 | awk '/inet / { print $2 }' | sed 's/addr://')
PRIV_IP=$(/sbin/ifconfig eth0:1 | awk '/inet / { print $2 }' | sed 's/addr://')
#GW="gateway"
NAME=$HOSTNAME


#modify ifconfig
set_ifconfig(){
mkdir -p /data/apps/bash-scripts/
cat >/data/apps/bash-scripts/ifconfig.sh<<EOF
#!/bin/bash

pvt=\`ip addr show eth0 | grep "inet\\b" | awk '{print \$2}' | cut -d/ -f1|grep 192.168\`
pub=\`ip addr show eth0 | grep "inet\\b" | awk '{print \$2}' | cut -d/ -f1|grep -v 192.168\`


print_eth0(){
cat <<EOF1
eth0      Link encap:Ethernet  HWaddr 
          inet addr:\$pub
 
EOF1
}

print_eth0_1(){
cat <<EOF2
eth0:1    Link encap:Ethernet  HWaddr f2:
          inet addr:\$pvt  

EOF2
}

if [ "\$1" == "eth0" ];then
	print_eth0
fi

if [ "\$1" == "eth0:1" ];then
	print_eth0_1
fi

if [ ! \$1 ];then
	print_eth0
	print_eth0_1
fi

EOF
chmod +x /data/apps/bash-scripts/ifconfig.sh
mv /sbin/ifconfig /sbin/ifconfig.orig
ln -s /data/apps/bash-scripts/ifconfig.sh /sbin/ifconfig
ifconfig
}

system_update(){
systemctl enable systemd-networkd
put_title "System Update"
put_msg "Press any key to continue"
#read
apt_opt="-o Acquire::ForceIPv4=true"
apt-get update $apt_opt
apt-get install vim python resolvconf -y $apt_opt
put_msg "Updates completed"
#read
}



#Install NRPE
install_nrpe(){
apt-get install openssl nagios-nrpe-server nagios-plugins nagios-plugins-basic nagios-plugins-standard resolvconf -y

echo "domain adgebra.int" >> /etc/resolvconf/resolv.conf.d/head
echo "search adgebra.int" >> /etc/resolvconf/resolv.conf.d/head
echo "nameserver 192.168.146.58" >> /etc/resolvconf/resolv.conf.d/head
echo "nameserver 192.168.157.50" >> /etc/resolvconf/resolv.conf.d/head

systemctl enable resolvconf

cat >/etc/nagios/nrpe.cfg<<EOF

log_facility=daemon
pid_file=/var/run/nagios/nrpe.pid
server_port=5666
nrpe_user=nagios
nrpe_group=nagios
allowed_hosts=127.0.0.1,203.109.101.177,203.109.101.178,192.168.196.78,172.104.178.22

dont_blame_nrpe=1
allow_bash_command_substitution=0
debug=0
command_timeout=60
connection_timeout=300
command[check_local_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
command[check_local_load]=/usr/lib/nagios/plugins/check_load -w \$ARG1\$ -c \$ARG2\$
command[check_local_part]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /dev/hda1
command[check_zombie_procs]=/usr/lib/nagios/plugins/check_procs -w 5 -c 10 -s Z
command[check_local_procs]=/usr/lib/nagios/plugins/check_procs -w \$ARG1\$ -c \$ARG2\$
command[check_local_disk]=/usr/lib/nagios/plugins/check_disk -w \$ARG1\$ -c \$ARG2\$ -p \$ARG3\$
command[check_mem]=/usr/lib/nagios/plugins/check_mem.pl -w \$ARG1\$ -c \$ARG2\$ -f -C
include=/etc/nagios/nrpe_local.cfg
include_dir=/etc/nagios/nrpe.d/
command[check_swap]=/usr/lib/nagios/plugins/check_swap -av -w \$ARG1$ -c \$ARG2$
command[check_oom_killer]=sudo /usr/lib/nagios/plugins/check_oom_killer.sh
command[check_ntp_time]=/usr/lib/nagios/plugins/check_ntp_time -H pool.ntp.org -w \$ARG1$ -c \$ARG2$
command[check_mysql]=/usr/lib/nagios/plugins/check_mysql -u $ARG1$ -p $ARG2$
EOF

cd /usr/lib/nagios/plugins/
wget https://cdn.adgebra.in/INFRA/check_mem.pl
chmod +x check_mem.pl
/etc/init.d/nagios-nrpe-server restart
cd -
}


# Install prometheus node_exporter
install_prom_exporter(){
cd /usr/local/src/
wget https://github.com/prometheus/node_exporter/releases/download/v0.18.0/node_exporter-0.18.0.linux-amd64.tar.gz
tar xvf node_exporter-*.linux-amd64.tar.gz
cp node_exporter-*.linux-amd64/node_exporter /usr/local/bin/
cat >/etc/systemd/system/node_exporter.service<<EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF

useradd node_exporter
chown node_exporter:node_exporter /usr/local/bin/node_exporter
systemctl daemon-reload
systemctl start node_exporter
systemctl enable node_exporter
}

set_hostname(){
put_title "Set Hostname"
echo $HOSTNAME > /etc/hostname
hostname -F /etc/hostname
echo "$PRIV_IP $HOSTNAME" >> /etc/hosts
}

#read

ntp_install(){
put_title "Configuring NTP service"
apt-get install ntp -y $apt_opt
service ntp restart
}

disk_alerts(){
put_title "Configure Disk alert"
#read
mkdir -p /data/apps/python/workspace/emailalerts

cd /data/apps/python/
wget https://cdn.adgebra.in/INFRA/emailSend_smtp.py
chmod +x emailSend_smtp.py
cp emailSend_smtp.py /data/apps/python/workspace/emailalerts/

cat >/data/apps/python/workspace/emailalerts/constants.py<<EOF
__author__ = 'rajeshchande'

INUNAME = 'rajesh'

key = 'key-27qv0v3jb-56ct-ss3x6-2ne3jz106y0'
sandbox = 'sandbox8d8bdd3148bd41a4b25ef6484a72e969.mailgun.org'
request_url = 'https://api.mailgun.net/v2/{0}/messages'.format(sandbox)
recipient = 'rajesh@inuxu.media'
fromEmail = 'infra@inuxu.media'
toEmail= 'rajesh@inuxu.media'
subject = 'Test Email'
bodyText = 'Hello from Mailgun'
EOF
python /data/apps/python/workspace/emailalerts/constants.py
chmod +x /data/apps/python/workspace/emailalerts/constants.py

cd /data/apps/python/workspace/emailalerts/
wget https://cdn.adgebra.in/INFRA/diskAlert.sh
chmod +x diskAlert.sh

put_msg "Disk alert configured"
#read

put_title "Test disk alert"
#read
sed -i "s/if(s >= 80):/if(s >= 0):/g" /data/apps/python/workspace/emailalerts/diskAlert.py
python /data/apps/python/workspace/emailalerts/diskAlert.py
sed -i "s/if(s >= 0):/if(s >= 80):/g" /data/apps/python/workspace/emailalerts/diskAlert.py
put_msg "test email sent"

put_title "Crontab entries"
#read
#write out current crontab
crontab -l > mycron
#echo new cron into cron file
echo "1 */2 * * * /data/apps/python/workspace/emailalerts/diskAlert.sh" >> mycron
#install new cron file
crontab mycron
rm mycron
crontab -l
put_msg "Crontab installed"
}

set_sysctl(){
put_title "Configuring sysctl.conf"
#read
mkdir /backup
cp /etc/sysctl.conf /backup/
cp sysctl-rajesh.conf /etc/sysctl.conf
sysctl -p
put_msg "Configuring sysctl.conf -- completed"

put_title "Configuring ulimits"
#read
ulimit -i 32063
ulimit -u 32063
ulimit -n 65535

cat >>/etc/security/limits.conf<<EOF
root             soft    nofile          65536
root             hard    nofile          65536
root    hard    sigpending      32063
root    soft    sigpending      32063
root    hard    nproc   32063
root    soft    nproc   32063

*             soft    nofile          65536
*             hard    nofile          65536
*   hard    sigpending      32063
*    soft    sigpending      32063
*    hard    nproc   32063
*    soft    nproc   32063
EOF

put_msg "Configuring ulimits -- completed"
#read
}

disable_ipv6(){
put_title "Disable ipv6"
#read
cat >/etc/network/if-pre-up.d/firewall<<EOF
#!/bin/sh
/sbin/iptables-restore < /etc/iptables.firewall.rules
ip6tables -I INPUT -j DROP
EOF
chmod +x /etc/network/if-pre-up.d/firewall
ip6tables -I INPUT -j DROP
put_msg "-- completed"
}

set_iptables(){
put_title "Configuring iptables"
cat >/etc/iptables.firewall.rules<<EOF

# Modified date : `date`
# Created / Modified by : Aniruddha K.
*security
:INPUT ACCEPT [786385:198741883]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [761854:140434590]
COMMIT
#
#
*raw
:PREROUTING ACCEPT [786560:198752557]
:OUTPUT ACCEPT [761854:140434590]
COMMIT
# Completed on Mon Mar 23 11:21:25 2015
# Generated by iptables-save v1.4.21 on Mon Mar 23 11:21:25 2015
*nat
:PREROUTING ACCEPT [121:7410]
:INPUT ACCEPT [78:4672]
:OUTPUT ACCEPT [137:8615]
:POSTROUTING ACCEPT [137:8615]
COMMIT
#
#
*mangle
:PREROUTING ACCEPT [786560:198752557]
:INPUT ACCEPT [786560:198752557]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [761854:140434590]
:POSTROUTING ACCEPT [761854:140434590]
COMMIT
#
#
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [62:3912]
:LOGGING - [0:0]
-A INPUT -s 203.109.101.177 -i eth0 -p tcp -m tcp -m state --state NEW,ESTABLISHED -m comment --comment "Allow Incoming from a Office IP" -j ACCEPT
-A INPUT -s 203.109.101.178 -i eth0 -p tcp -m tcp -m state --state NEW,ESTABLISHED -m comment --comment "Allow Incoming from a Office IP" -j ACCEPT
-A INPUT -s 192.168.196.78 -i eth0 -p tcp -m tcp -m state --state NEW,ESTABLISHED -m comment --comment "Allow Incoming from a nagwatch" -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --sport 22 -m state --state ESTABLISHED -m comment --comment "Allow Outgoing SSH" -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --sport 80 -m state --state ESTABLISHED -m comment --comment "Allow Outgoing HTTP" -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --sport 443 -m state --state ESTABLISHED -m comment --comment "Allow Outgoing HTTPS" -j ACCEPT
-A INPUT -i eth0 -p tcp -m tcp --sport 3306 -m state --state ESTABLISHED -m comment --comment "Allow Outgoing MySQL" -j ACCEPT
-I INPUT -s 192.168.147.160 -i eth0 -m state --state NEW,ESTABLISHED -m comment --comment " Allow sg-doc-reg " -j  ACCEPT
#
#-A INPUT -i eth0 -p tcp -m tcp -m state --state ESTABLISHED -m comment --comment " Allow Outgoing " -j ACCEPT
#
# Append Custom rules here
#STRT
#END
-A INPUT -p icmp -m icmp --icmp-type 8 -m comment --comment "Allow Ping from Outside to Inside" -j ACCEPT
-A INPUT -p icmp -m icmp --icmp-type 0 -m comment --comment "Allow Ping from Inside to Outside" -j ACCEPT
-A INPUT -i lo -m comment --comment "Allow Loopback Access" -j ACCEPT
-A INPUT -i eth0 -p udp -m udp --sport 53 -m comment --comment "Allow outbound DNS" -j ACCEPT
-A INPUT -i eth0 -p udp -m udp --sport 53 -m conntrack --ctstate RELATED,ESTABLISHED -m comment --comment "Allow outbound DNS" -j ACCEPT
-A OUTPUT -p udp --dport 123 -m comment --comment "Allow outbound ntp" -j ACCEPT
-A INPUT -p udp --sport 123 -m comment --comment "Allow outbound ntp" -j ACCEPT
-A INPUT -p tcp -m tcp --dport 80 -m limit --limit 25/min --limit-burst 100 -m comment --comment "Prevent DoS Attack" -j ACCEPT
-A INPUT -j LOGGING
-A OUTPUT -o eth0 -p tcp -m tcp --sport 22 -m state --state ESTABLISHED -m comment --comment " SSH " -j ACCEPT
-A OUTPUT -o eth0 -p tcp -m tcp --dport 22 -m state --state NEW,ESTABLISHED -m comment --comment "Allow Outgoing SSH" -j ACCEPT
-A OUTPUT -o eth0 -p tcp -m tcp --dport 80 -m state --state NEW,ESTABLISHED -m comment --comment "Allow Outgoing HTTP" -j ACCEPT
-A OUTPUT -o eth0 -p tcp -m tcp --dport 443 -m state --state NEW,ESTABLISHED -m comment --comment "Allow Outgoing HTTPS" -j ACCEPT
-A OUTPUT -o eth0 -p tcp -m tcp --dport 3306 -m state --state NEW,ESTABLISHED -m comment --comment "Allow Outgoing MySQL" -j ACCEPT
#
# Allow all outbound traffic
#-A OUTPUT -o eth0 -p tcp -m tcp -m state --state NEW,ESTABLISHED -m comment --comment " Allow Outgoing " -j ACCEPT
#
# Append Custom rules here
#START
#
#END
-A OUTPUT -p icmp -m icmp --icmp-type 0 -m comment --comment "Allow Ping from Outside to Inside" -j ACCEPT
-A OUTPUT -p icmp -m icmp --icmp-type 8 -m comment --comment "Allow Ping from Inside to Outside" -j ACCEPT
-A OUTPUT -o lo -m comment --comment "Allow Loopback Access" -j ACCEPT
-A OUTPUT -o eth0 -p udp -m udp --dport 53 -m comment --comment "Allow outbound DNS" -j ACCEPT
-A OUTPUT -o eth0 -p udp -m udp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "Allow outbound DNS" -j ACCEPT
-A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables Packet Dropped: Afte" --log-level 7
-A LOGGING -j DROP
COMMIT

EOF

echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo debconf-set-selections
apt install iptables-persistent -y
ln -s /etc/iptables.firewall.rules /etc/iptables/rules.v4
}

set_ifconfig
system_update
#install_nrpe
#install_prom_exporter
#set_hostname
ntp_install
disk_alerts
disable_ipv6
#set_iptables
set_sysctl

put_title "Please Reboot the server now ..."


