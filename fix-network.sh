#!/bin/bash

sudo systemctl disable networking.service

sudo systemctl disable resolvconf.service 

sudo systemctl enable systemd-networkd.service

sudo systemctl enable systemd-resolved.service

apt-get remove resolvconf -y

echo "DNS=192.168.146.58 192.168.157.50" >> /etc/systemd/resolved.conf

>/etc/network/interfaces

