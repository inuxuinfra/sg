#!/bin/bash

docker run --name kibana2 -d -e ELASTICSEARCH_REQUESTTIMEOUT="3000000" -e ELASTICSEARCH_HOSTS=http://sg-ELK-1.adgebra.int:9200 -e ELASTICSEARCH_HOSTS=http://sg-ELK-2.adgebra.int:9200 -p 5601:5601 kibana:7.3.0
