#!/bin/bash

for i in `cat srvlist.txt`;do
	hostname=`echo $i|cut -d: -f3`
	ip=`echo $i|cut -d: -f1`
	cpu=`ssh root@$ip cat /proc/cpuinfo |grep "model name"|head -1|cut -d: -f2`
	echo "$hostname,$cpu"
done
