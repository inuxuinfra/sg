#!/bin/bash
apt_opt="-o Acquire::ForceIPv4=true"
apt update $apt_opt
apt install $apt_opt \
apt-transport-https \
ca-certificates \
curl \
software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"
apt update $apt_opt
apt install docker-ce -y  $apt_opt

