#!/bin/bash

ip=`/sbin/ifconfig eth0|grep inet|cut -d: -f2|awk '{print $1}'`
sendEmail(){
        /data/apps/python/workspace/emailalerts/emailSend_smtp.py \
        "infra@inuxu.media" \
        "`date` - $scriptName is already running." \
        "`env TZ=Asia/Kolkata date` - $scriptName is already running on `hostname` / $ip


Inconvenience regretted.
Adgebra AI" \
        ""
}

notify_error(){
        /data/apps/python/workspace/emailalerts/emailSend_smtp.py \
        "infra@inuxu.media,qa@inuxu.media" \
        "`date` - cron scripts errors / missing / deleted" \
        "`env TZ=Asia/Kolkata date` - some crontab scripts are missing `hostname` / $ip
        Please check attachment

Inconvenience regretted.
Adgebra AI" \
        "$issuesfile"
}

scriptName=$(basename $0)
for pid in $(pidof -x $scriptName); do
   if [ $pid != $$ ]; then
       echo "[$(date)] : $scriptName : Process is already running with PID $pid"
       sendEmail
       exit 1
   fi
done

workdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
mkdir -p $workdir/cron-monitor/
cronlist=$workdir/cron-monitor/cron-list.txt
issuesfile=$workdir/cron-monitor/issue-list.txt


crontab -l|egrep -v '^[[:space:]]*$|^ *#' |awk '{print $6}'|sed '/^bash/d'|sed '/^time/d' > $cronlist
crontab -l|awk '{print $7}'|egrep -v '^[[:space:]]*$|^ *#' |grep "^/" >> $cronlist
>$issuesfile
for i in `cat $cronlist`; do
  ls $i > /dev/null 2>&1;
  if [ $? -ne 0 ];then
    echo "$i" >> $issuesfile;
  fi;
done
if [ `wc -l $issuesfile|awk '{print $1}'` -gt 0 ];then
  notify_error
fi

